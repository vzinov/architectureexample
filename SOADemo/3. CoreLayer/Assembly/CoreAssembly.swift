//
//  CoreAssembly.swift
//  SOADemo
//

import Foundation

protocol ICoreAssembly {
    var diskImageStorage: IImageStorage { get }
    var secureImageStorage: IImageStorage { get }
}

class CoreAssembly: ICoreAssembly {
    lazy var diskImageStorage: IImageStorage = ImageDiskStorage()
    lazy var secureImageStorage: IImageStorage = SecureImageStorage()
}
