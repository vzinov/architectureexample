//
//  SecureImageStorage.swift
//  SOADemo
//

import UIKit

class SecureImageStorage: IImageStorage {
    func save(image: UIImage, for key: String) {
        print("\(#function)")
    }
    
    func fetchImage(key: String) -> UIImage? {
        print("\(#function)")
        return nil
    }
}
