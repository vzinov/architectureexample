//
//  DemoAssembly.swift
//  SOADemo
//

import Foundation
import UIKit

protocol IPresentationAssembly {
    /// Создает экран с приложениями
    func demoViewCotnroller() -> DemoViewController
    
    /// Создает экран где бегают пингвинчики
    func pinguinViewController() -> PinguinViewController
}

class PresentationAssembly: IPresentationAssembly {
    
    private let serviceAssembly: IServicesAssembly
    
    init(serviceAssembly: IServicesAssembly) {
        self.serviceAssembly = serviceAssembly
    }
    
    // MARK: - DemoViewController
    
    func demoViewCotnroller() -> DemoViewController {
        let demoVC = DemoViewController(presentationAssembly: self,
                                        tracksService: serviceAssembly.tracksService,
                                        cardsService: serviceAssembly.cardsService)
        return demoVC
    }
    
    // MARK: - PinguinViewController
    
    func pinguinViewController() -> PinguinViewController {
        return PinguinViewController()
    }
    
    // MARK: - AnotherViewController
    //.....
}
