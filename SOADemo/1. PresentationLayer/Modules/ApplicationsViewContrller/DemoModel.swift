//
//  DemoModel.swift
//  SOADemo
//

import UIKit
import Foundation

struct DemoCellDisplayModel {
    let title: String
    let imageUrl: String
}
