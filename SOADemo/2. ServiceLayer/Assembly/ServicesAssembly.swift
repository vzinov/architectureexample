//
//  ServicesAssembly.swift
//  SOADemo
//

import Foundation

protocol IServicesAssembly {
    var tracksService: ITracksService { get }
    var cardsService: ICardsService { get }
}

class ServicesAssembly: IServicesAssembly {
    
    private let coreAssembly: ICoreAssembly
    
    init(coreAssembly: ICoreAssembly) {
        self.coreAssembly = coreAssembly
    }
    
    lazy var tracksService: ITracksService = TracksService(imageStorage: self.coreAssembly.secureImageStorage)
    lazy var cardsService: ICardsService = CardsService(imageStorage: self.coreAssembly.diskImageStorage)
}
