//
//  TracksService.swift
//  SOADemo
//

import Foundation

protocol ITracksService {
    func fetchTopTracks()
}

class TracksService: ITracksService {
    
    let imageStorage: IImageStorage
    
    init(imageStorage: IImageStorage) {
        self.imageStorage = imageStorage
    }
    
    func fetchTopTracks() {
        let image = imageStorage.fetchImage(key: "_")
        print(image as Any)
    }
}
