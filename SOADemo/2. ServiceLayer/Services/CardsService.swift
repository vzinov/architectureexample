//
//  File.swift
//  SOADemo
//

import Foundation

protocol ICardsService {
    func fetchNewApps()
}

class CardsService: ICardsService {
    
    let imageStorage: IImageStorage

    init(imageStorage: IImageStorage) {
        self.imageStorage = imageStorage
    }
    
    func fetchNewApps() {
        let image = imageStorage.fetchImage(key: "_")
        print(image as Any)
    }
    
}
